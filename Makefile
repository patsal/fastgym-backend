init:
	@go get -u github.com/Masterminds/glide
	@glide install
run:
	@go run main.go
fmt:
	@go fmt ./...

