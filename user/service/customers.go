package service

import "bitbucket.org/patsal/fastgym-backend/user/api"
import (
	"bitbucket.org/patsal/fastgym-backend/user/data"
	"errors"
	"github.com/davecgh/go-spew/spew"
	"math/rand"
	"strconv"
	"strings"
)

type CustomersService interface {
	GetCustomers() ([]api.Customer, error)
	GetCustomersInfo() ([]api.CustomerInfo, error)
	CreateCustomer(customer api.Customer) error
	CreateCustomerInfo(customerInfo api.CustomerInfo) error
	NewCustomerInfo(username string, accountType string, valid string, balance string, pushId string) (api.CustomerInfo, error)
}

type customersService struct{}

func (s customersService) GetCustomers() ([]api.Customer, error) {
	return data.GetCustomers()
}

func (s customersService) GetCustomersInfo() ([]api.CustomerInfo, error) {
	return data.GetCustomersInfo()
}

func (s customersService) CreateCustomer(customer api.Customer) error {
	if s.isExistingCustomer(customer.UserName) {
		return errors.New(customer.UserName + " username already exists")
	}
	customer.UserName = sanitizeUsername(customer.UserName)
	err := data.CreateCustomer(customer)
	if err != nil {
		return err
	}

	return nil
}

func (s customersService) CreateCustomerInfo(customerInfo api.CustomerInfo) error {
	/*	if s.isExistingCustomerInfo(customerInfo.UserName) {
		return errors.New(customerInfo.UserName + " username already exists")
	}*/
	customerInfo.UserName = sanitizeUsername(customerInfo.UserName)
	err := data.CreateCustomerInfo(customerInfo)
	if err != nil {
		return err
	}

	return nil
}

/*func (s customersService) CreateCustomerInfo(customerInfo api.CustomerInfo) error {

		if s.isExistingCustomerInfo(customerInfo.UserName) {
			return errors.New(customerInfo.UserName + " username already exists")
		}
		customerInfo.UserName = sanitizeUsername(customerInfo.UserName)
		err := data.CreateCustomerInfo(customerInfo,nil)
		if err != nil {
			return err
		}

	return nil
}*/

/*func (s customersService) CreateCustomers(customers []api.Customer) error {
	for _, newCustomer := range customers {
		if s.isExistingCustomer(newCustomer.UserName) {
			return errors.New(newCustomer.UserName + "username already exists")
		}
		newCustomer.UserName = sanitizeUsername(newCustomer.UserName)
		err := data.CreateCustomer(newCustomer)
		if err != nil {
			return err
		}
	}
	return nil
}*/

func (s customersService) isExistingCustomerInfo(username string) bool {
	customersInDb, _ := s.GetCustomersInfo()
	for index, customerInDb := range customersInDb {
		spew.Dump(index)
		spew.Dump(customerInDb.UserName)
		spew.Dump(strings.ToLower(strings.Trim(username, " ")))
		if customerInDb.UserName == sanitizeUsername(username) {
			return true
		}
	}
	return false
}

func (s customersService) isExistingCustomer(username string) bool {
	customersInDb, _ := s.GetCustomers()
	for index, customerInDb := range customersInDb {
		spew.Dump(index)
		spew.Dump(customerInDb.UserName)
		spew.Dump(strings.ToLower(strings.Trim(username, " ")))
		if customerInDb.UserName == sanitizeUsername(username) {
			return true
		}
	}
	return false
}

func NewCustomersService() CustomersService {
	//later on if MySqlDb dependency not present, throw error or panic
	return customersService{}
}

func (s customersService) NewCustomerInfo(username string, accountType string, valid string, balance string, pushId string) (api.CustomerInfo, error) {
	customerInfo := api.CustomerInfo{
		UserName:    username,
		AccountType: accountType,
		Valid:       valid,
		Balance:     balance,
		QrCode:      getQRCode(),
		PushId:      pushId,
	}
	return customerInfo, nil
}

func getQRCode() string {
	qc := strconv.Itoa(rand.Intn(999999999999999999))
	return qc
}

func sanitizeUsername(username string) string {
	return strings.ToLower(strings.Trim(username, " "))
}
