package service

import "bitbucket.org/patsal/fastgym-backend/user/api"
import "bitbucket.org/patsal/fastgym-backend/user/data"

type GymsService interface {
	GetGymInfo() ([]api.GymInfo, error)
}

type gymsService struct{}

func (s gymsService) GetGymInfo() ([]api.GymInfo, error) {
	return data.GetGymInfo()
}

func NewGymsService() GymsService {
	//later on if MySqlDb dependency not present, throw error or panic
	return gymsService{}
}
