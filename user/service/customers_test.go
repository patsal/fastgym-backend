package service

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"testing"
)

func TestCustomersService_CreateCustomerSuccessfulCreation(t *testing.T) {
	customersService := NewCustomersService()
	customer := api.Customer{
		Firstname: "John",
		Lastname:  "Smith",
		UserName:  "john123",
		Password:  "123",
		Address:   "Lebanon",
	}
	returnedError := customersService.CreateCustomer(customer)
	if returnedError != nil {
		t.Errorf("function returned error instead of nil")
	}
}
