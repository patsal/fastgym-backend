package service

import "bitbucket.org/patsal/fastgym-backend/user/api"
import "bitbucket.org/patsal/fastgym-backend/user/data"

type MembershipService interface {
	GetDiscountInfo() ([]api.Discount, error)
	GetMembershipInfo() ([]api.Membership, error)
}

type membershipService struct{}

func (s membershipService) GetDiscountInfo() ([]api.Discount, error) {
	return data.GetDiscountInfo()
}

func (s membershipService) GetMembershipInfo() ([]api.Membership, error) {
	return data.GetMembershipInfo()
}

func NewMembershipService() MembershipService {
	//later on if MySqlDb dependency not present, throw error or panic
	return membershipService{}
}
