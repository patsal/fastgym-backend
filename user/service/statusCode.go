package service

import "bitbucket.org/patsal/fastgym-backend/user/api"
import "bitbucket.org/patsal/fastgym-backend/user/data"

type StatusCodeService interface {
	GetStatusCode() ([]api.StatusCode, error)
	NewStatusCode(code int, message string) api.StatusCode
}

type statusCodeService struct{}

func (s statusCodeService) GetStatusCode() ([]api.StatusCode, error) {
	return data.GetStatusCode()
}

func (s statusCodeService) NewStatusCode(code int, message string) api.StatusCode {
	return api.StatusCode{
		StatusCodeList: api.StatusCodeList{
			Code:    code,
			Message: message,
		},
	}
}

func NewStatusCodeService() StatusCodeService {
	//later on if MySqlDb dependency not present, throw error or panic
	return statusCodeService{}
}
