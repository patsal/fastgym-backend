package service

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"bitbucket.org/patsal/fastgym-backend/user/data"
)

type RechargeService interface {
	GetCardInfo() ([]api.CardInfo, error)
	DeleteCardInfo(cardInfo api.CardInfo) error
}

type rechargeService struct{}

func (s rechargeService) GetCardInfo() ([]api.CardInfo, error) {
	return data.GetCardInfo()
}

func (s rechargeService) DeleteCardInfo(cardInfo api.CardInfo) error {
	/*	if s.isExistingCustomerInfo(customerInfo.UserName) {
		return errors.New(customerInfo.UserName + " username already exists")
	}*/
	cardInfo.UserName = sanitizeUsername(cardInfo.CardId)
	err := data.DeleteCardRecharge(cardInfo)
	if err != nil {
		return err
	}

	return nil
}

func NewRechargeService() RechargeService {
	//later on if MySqlDb dependency not present, throw error or panic
	return rechargeService{}
}
