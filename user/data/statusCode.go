package data

import "bitbucket.org/patsal/fastgym-backend/user/api"

func GetStatusCode() ([]api.StatusCode, error) {
	//Mock data - implement gym information
	var statusCode []api.StatusCode
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 0, Message: "Success"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 1, Message: "wrong username or password"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 2, Message: "Registration completed successfully"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 3, Message: "Username Already Exist"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 4, Message: "Failed to load userinfo"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 5, Message: "Bad Request - JSON cannot be parsed"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 6, Message: "Wrong card id"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 7, Message: "Your account has been recharged successfully"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 8, Message: "Bad Request"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 9, Message: "You do not have enough balance"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 10, Message: "You have to wait for offer expiration"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 11, Message: "User notified successfully"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 12, Message: "Unknown profile"}})
	statusCode = append(statusCode, api.StatusCode{StatusCodeList: api.StatusCodeList{Code: 13, Message: "Account expired"}})
	return statusCode, nil
}
