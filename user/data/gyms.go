package data

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
)

func GetGymInfo() ([]api.GymInfo, error) {

	var gymInfo []api.GymInfo
	var atlantis = "/user/static/atlantis.jpg"
	gymInfo = append(gymInfo, api.GymInfo{GymName: "Atlantis", GymLocation: "kaslik، Jounieh", GymId: "0017", GymTime: "Mo-Fri(8am-11pm)| Sat(8am-9pm)", GymNumber: "70660017", GymX: "33.977453", GymY: "35.612347", Image: atlantis})
	gymInfo = append(gymInfo, api.GymInfo{GymName: "ClassSport", GymLocation: "Mansourieh Highway mount lebanon، Class Sport Building، Mansouriyeh", GymId: "0670", GymTime: "Mo-Sat(9am-9pm)| Sun(9am-3pm)", GymNumber: "01680670", GymX: "33.858335", GymY: "35.559622", Image: atlantis})
	gymInfo = append(gymInfo, api.GymInfo{GymName: "FitnessZone", GymLocation: "Dbayeh", GymTime: "Mo-Fri(6am-11pm)| Sat-Sun(8am-8pm)", GymId: "3433", GymNumber: "04543433", GymX: "33.937989", GymY: "35.590506", Image: atlantis})

	return gymInfo, nil
}
