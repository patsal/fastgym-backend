package data

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"fmt"
	"github.com/davecgh/go-spew/spew"
)

var cardInfo []api.CardInfo

func init() {
	setCardInfo()
	fmt.Println("CardRecharge module initializing.... OK")
}

func GetCardInfo() ([]api.CardInfo, error) {
	//Mock data - implement gym information
	return cardInfo, nil
}

func setCardInfo() ([]api.CardInfo, error) {
	//Mock data - implement gym information
	cardInfo = append(cardInfo, api.CardInfo{CardId: "basic578as648x1", CardType: "basic"})
	cardInfo = append(cardInfo, api.CardInfo{CardId: "basic578as648x2", CardType: "basic"})
	cardInfo = append(cardInfo, api.CardInfo{CardId: "basic444xs668x3", CardType: "basic"})
	cardInfo = append(cardInfo, api.CardInfo{CardId: "gold5123as245x4", CardType: "gold"})
	cardInfo = append(cardInfo, api.CardInfo{CardId: "gold5578as245x5", CardType: "gold"})
	cardInfo = append(cardInfo, api.CardInfo{CardId: "diam55as8s654x4", CardType: "diamond"})
	cardInfo = append(cardInfo, api.CardInfo{CardId: "xxxx11as2z654x4", CardType: "xxxx"})
	cardInfo = append(cardInfo, api.CardInfo{CardId: "xxxx15gs2z654x4", CardType: "xxxx"})
	return cardInfo, nil
}

func DeleteCardRecharge(CardInfo api.CardInfo) error {
	for index, item := range cardInfo {
		if item.CardId == CardInfo.CardId {
			spew.Dump("true")
			cardInfo = append(cardInfo[:index], cardInfo[index+1:]...)

		}
	}
	return nil
}
