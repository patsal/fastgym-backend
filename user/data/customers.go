package data

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"fmt"
	"github.com/davecgh/go-spew/spew"
)

var customers []api.Customer
var customersInfo []api.CustomerInfo

func init() {
	setCustomers()
	setCustomersInfo()
	fmt.Println("Customer and customerInfo Data module initializing.... OK")
}

func GetCustomers() ([]api.Customer, error) {
	// Mock data - implement MySqlDb
	return customers, nil
}

func setCustomers() error {
	customers = append(customers, api.Customer{Firstname: "Patrick", Lastname: "Saber", UserName: "pat123", Password: "12345678", Address: "Lebanon"})
	customers = append(customers, api.Customer{Firstname: "Salim", Lastname: "Saber", UserName: "selim_as", Password: "123456", Address: "Germany"})
	customers = append(customers, api.Customer{Firstname: "Joseph", Lastname: "Aalam", UserName: "Joseph.sadek", Password: "123456", Address: "Germany"})
	return nil
}

func GetCustomersInfo() ([]api.CustomerInfo, error) {

	return customersInfo, nil
}

func setCustomersInfo() error {

	customersInfo = append(customersInfo, api.CustomerInfo{UserName: "pat123", AccountType: " ", Valid: " ", Balance: " ", QrCode: "999999999999999999"})
	customersInfo = append(customersInfo, api.CustomerInfo{UserName: "selim_as", AccountType: " ", Valid: " ", Balance: "200", QrCode: "999999999999999998"})
	customersInfo = append(customersInfo, api.CustomerInfo{UserName: "joseph.sadek", AccountType: " ", Valid: " ", Balance: "40", QrCode: "999999999999999997"})
	return nil
}

func CreateCustomer(customer api.Customer) error {
	customers = append(customers, customer)
	return nil
}
func CreateCustomerInfo(customerInfo api.CustomerInfo) error {
	for index, item := range customersInfo {
		if item.UserName == customerInfo.UserName {
			spew.Dump("true")
			customersInfo = append(customersInfo[:index], customersInfo[index+1:]...)
		}

	}

	customersInfo = append(customersInfo, customerInfo)

	return nil
}
