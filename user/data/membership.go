package data

import "bitbucket.org/patsal/fastgym-backend/user/api"

func GetDiscountInfo() ([]api.Discount, error) {
	//Mock data - implement gym information
	var discountInfo []api.Discount
	discountInfo = append(discountInfo, api.Discount{Id: "1", Title: "5 months membership", DateExpiration: "Valid till November 8", Price: "200 POINTS", Priority: "diamond offer"})
	discountInfo = append(discountInfo, api.Discount{Id: "2", Title: "4 months membership", DateExpiration: "Valid till November 8", Price: "130 POINTS", Priority: "gold offer"})
	return discountInfo, nil
}

func GetMembershipInfo() ([]api.Membership, error) {
	//Mock data - implement gym information
	var membershipInfo []api.Membership
	membershipInfo = append(membershipInfo, api.Membership{Id: "1", Title: "3 months membership", Price: "150 POINTS", Priority: "diamond"})
	membershipInfo = append(membershipInfo, api.Membership{Id: "2", Title: "2 months membership", Price: "90 POINTS", Priority: "gold"})
	membershipInfo = append(membershipInfo, api.Membership{Id: "3", Title: "1 months membership", Price: "40 POINTS", Priority: "basic"})
	return membershipInfo, nil
}
