package calls

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"bitbucket.org/patsal/fastgym-backend/user/data"
	"encoding/json"
	"github.com/davecgh/go-spew/spew"
	"github.com/gorilla/mux"
	"net/http"
)

func GetCustomerInfo(w http.ResponseWriter, router *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	var t api.Customer
	err := json.NewDecoder(router.Body).Decode(&t)
	if err != nil {
		json.NewEncoder(w).Encode(statusCodes[5])
		return
	}
	spew.Dump(t)
	customers, _ := customerService.GetCustomersInfo()
	statusCode, _ := statusCodeService.GetStatusCode()
	for _, item := range customers {
		if item.UserName == t.UserName {
			response := api.Response{
				ResponseStatus: api.ResponseStatus{
					Code:    0,
					Message: "Success",
				},
				Data: item,
			}

			json.NewEncoder(w).Encode(response)

			return
		}
	}

	json.NewEncoder(w).Encode(statusCode[4])
}

func GetCustomersInfo(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	if len(customerInfo) > 0 {
		response := api.Response{
			ResponseStatus: api.ResponseStatus{
				Code:    0,
				Message: "Success",
			},
			Data: customerInfo,
		}
		json.NewEncoder(w).Encode(response)
		return
	}

}

func GetCustomer(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(router) //get Params

	for _, item := range customers {
		if item.UserName == params["userName"] {
			json.NewEncoder(w).Encode(&item)
			return
		}
	}
	json.NewEncoder(w).Encode(&api.Customer{})
}

func GetCustomers(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	customers, _ := data.GetCustomers()
	json.NewEncoder(w).Encode(&customers)
}
