package calls

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func BuyMembership(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var t api.CardInfo
	var valid = " "
	var balance = " "
	var cardType = " "

	var a = false
	layout := "01-02-2006"

	err := json.NewDecoder(router.Body).Decode(&t)
	if err != nil {
		json.NewEncoder(w).Encode(statusCodes[5])
		return
	}

	for _, item := range customerInfo {

		if item.UserName == t.UserName {
			var pushId = router.Header.Get("pushId")

			if item.AccountType == "Diamond offer" || item.AccountType == "Gold offer" {
				json.NewEncoder(w).Encode(statusCodes[10])
				return
			}

			if strings.Contains(t.CardType, "diamond offer") {

				valid = currentTime.AddDate(0, 5, 0).Format(layout)
				bt, err := strconv.Atoi(item.Balance)
				if err != nil {

					json.NewEncoder(w).Encode(statusCodes[8])

					return
				} else if bt < 200 {

					json.NewEncoder(w).Encode(statusCodes[9])
					return

				} else if item.Valid != " " {
					layout := "01-02-2006"
					t, _ := time.Parse(layout, item.Valid)
					valid = t.AddDate(0, 5, 0).Format(layout)
				}
				balance = strconv.Itoa(bt - 200)
				cardType = "Diamond offer"

			} else if strings.Contains(t.CardType, "gold offer") {

				valid = currentTime.AddDate(0, 4, 0).Format(layout)
				bt, err := strconv.Atoi(item.Balance)

				if err != nil {

					json.NewEncoder(w).Encode(statusCodes[8])
					return

				} else if bt < 130 {

					json.NewEncoder(w).Encode(statusCodes[9])
					return
				} else if item.Valid != " " {
					layout := "01-02-2006"
					t, _ := time.Parse(layout, item.Valid)
					valid = t.AddDate(0, 4, 0).Format(layout)

				}
				balance = strconv.Itoa(bt - 130)
				cardType = "Gold offer"

			} else if strings.Contains(t.CardType, "diamond") {

				valid = currentTime.AddDate(0, 3, 0).Format(layout)
				bt, err := strconv.Atoi(item.Balance)

				if err != nil {

					json.NewEncoder(w).Encode(statusCodes[8])
					return

				} else if bt < 150 {

					json.NewEncoder(w).Encode(statusCodes[9])
					return

				} else if item.Valid != " " {

					t, _ := time.Parse(layout, item.Valid)
					cardType = "Multi"
					a = true
					valid = t.AddDate(0, 3, 0).Format(layout)

				}

				balance = strconv.Itoa(bt - 150)
				if a == false {
					cardType = "Diamond"
				}

			} else if strings.Contains(t.CardType, "gold") {

				valid = currentTime.AddDate(0, 2, 0).Format(layout)
				bt, err := strconv.Atoi(item.Balance)
				if err != nil {
					json.NewEncoder(w).Encode(statusCodes[8])
					return

				} else if bt < 90 {
					json.NewEncoder(w).Encode(statusCodes[9])
					return

				} else if item.Valid != " " {
					layout := "01-02-2006"
					t, _ := time.Parse(layout, item.Valid)
					cardType = "Multi"
					a = true
					valid = t.AddDate(0, 2, 0).Format(layout)

				}
				balance = strconv.Itoa(bt - 90)
				if a == false {
					cardType = "Gold"
				}

			} else if strings.Contains(t.CardType, "basic") {

				valid = currentTime.AddDate(0, 1, 0).Format(layout)
				bt, err := strconv.Atoi(item.Balance)
				if err != nil {
					json.NewEncoder(w).Encode(statusCodes[8])
					return

				} else if bt < 40 {
					json.NewEncoder(w).Encode(statusCodes[9])
					return

				} else if item.Valid != " " {
					layout := "01-02-2006"
					t, _ := time.Parse(layout, item.Valid)
					cardType = "Multi"
					valid = t.AddDate(0, 1, 0).Format(layout)
				}

				balance = strconv.Itoa(bt - 40)
				if a == false {
					cardType = "Basic"
				}

			} else {
				json.NewEncoder(w).Encode(statusCodes[8])
				return
			}

			customerInfo, err := customerService.NewCustomerInfo(t.UserName, cardType, valid, balance, pushId)
			if err != nil {
				errMsg := err.Error()
				newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
				json.NewEncoder(w).Encode(&newStatusCode)
				return

			}
			customerService.CreateCustomerInfo(customerInfo)
			if err != nil {
				errMsg := err.Error()
				newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
				json.NewEncoder(w).Encode(&newStatusCode)
				return
			}

			json.NewEncoder(w).Encode(statusCodes[7])

			return
		}
	}

	json.NewEncoder(w).Encode(statusCodes[6])
}
