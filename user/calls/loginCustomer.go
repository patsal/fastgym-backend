package calls

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"encoding/json"
	"github.com/davecgh/go-spew/spew"
	"net/http"
)

func LoginUser(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var t api.Customer
	err := json.NewDecoder(router.Body).Decode(&t)
	if err != nil {
		json.NewEncoder(w).Encode(statusCodes[5])
		return
	}

	spew.Dump(t)

	for _, customer := range customers {
		if customer.UserName == t.UserName && customer.Password == t.Password {
			json.NewEncoder(w).Encode(statusCodes[0])

			return

		}
	}
	json.NewEncoder(w).Encode(statusCodes[1])
}
