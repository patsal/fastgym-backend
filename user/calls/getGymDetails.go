package calls

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"bitbucket.org/patsal/fastgym-backend/user/service"
	"encoding/json"
	"net/http"
)

var gymService = service.NewGymsService()

func GetGymDetails(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	response := api.Response{
		ResponseStatus: api.ResponseStatus{
			Code:    0,
			Message: "Success",
		},
		Data: gymsInfo,
	}

	json.NewEncoder(w).Encode(response)
}
