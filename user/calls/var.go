package calls

import (
	"time"
)

var currentTime = time.Now()
var statusCodes, _ = statusCodeService.GetStatusCode()
var customerInfo, _ = customerService.GetCustomersInfo()
var customers, _ = customerService.GetCustomers()

var discountInfo, _ = discountService.GetDiscountInfo()
var membershipInfo, _ = discountService.GetMembershipInfo()

var gymsInfo, _ = gymService.GetGymInfo()
