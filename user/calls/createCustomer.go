package calls

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"encoding/json"
	"github.com/davecgh/go-spew/spew"
	"net/http"
)

func CreateCustomer(w http.ResponseWriter, router *http.Request) {

	var pushId = router.Header.Get("pushId")
	var customer api.Customer

	err := json.NewDecoder(router.Body).Decode(&customer)
	if err != nil {
		json.NewEncoder(w).Encode(statusCodes[5])
		return
	}

	spew.Dump(customer)

	err = customerService.CreateCustomer(customer)
	if err != nil {
		errMsg := err.Error()
		newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
		json.NewEncoder(w).Encode(&newStatusCode)
		return
	}

	customerInfo, err := customerService.NewCustomerInfo(customer.UserName, " ", " ", "0", pushId)
	if err != nil {
		errMsg := err.Error()
		newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
		json.NewEncoder(w).Encode(&newStatusCode)
		return
	}

	customerService.CreateCustomerInfo(customerInfo)
	if err != nil {
		errMsg := err.Error()
		newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
		json.NewEncoder(w).Encode(&newStatusCode)
		return
	}

	//here everything is successfull
	json.NewEncoder(w).Encode(statusCodes[2])
}
