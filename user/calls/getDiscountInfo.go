package calls

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"bitbucket.org/patsal/fastgym-backend/user/service"
	"encoding/json"
	"net/http"
)

var discountService = service.NewMembershipService()

func GetDiscountInfo(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	discountInfo, _ := discountService.GetDiscountInfo()
	response := api.Response{
		ResponseStatus: api.ResponseStatus{
			Code:    0,
			Message: "Success",
		},
		Data: discountInfo,
	}
	json.NewEncoder(w).Encode(response)
}

func GetMembershipInfo(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	response := api.Response{
		ResponseStatus: api.ResponseStatus{
			Code:    0,
			Message: "Success",
		},
		Data: membershipInfo,
	}
	json.NewEncoder(w).Encode(response)
}
