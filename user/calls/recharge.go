package calls

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"bitbucket.org/patsal/fastgym-backend/user/service"
	"encoding/json"
	"github.com/davecgh/go-spew/spew"
	"net/http"
	"strings"
)

var rechargeService = service.NewRechargeService()
var statusCodeService = service.NewStatusCodeService()

var customerService = service.NewCustomersService()

func Recharge(w http.ResponseWriter, router *http.Request) {
	statusCodes, _ := statusCodeService.GetStatusCode()
	w.Header().Set("Content-Type", "application/json")
	var t api.CardInfo
	var valid = " "
	var balance = " "
	var cardType = " "
	var pushId = router.Header.Get("pushId")

	err := json.NewDecoder(router.Body).Decode(&t)
	if err != nil {
		json.NewEncoder(w).Encode(statusCodes[5])
		return
	}

	recharge, _ := rechargeService.GetCardInfo()
	for _, item := range recharge {
		if item.CardId == t.CardId {
			if strings.Contains(t.CardId, "basic") {
				//valid = currentTime.AddDate(0, 1, 0).Format("01-02-2006")
				valid = " "
				cardType = " "
				balance = "40"
				//cardType = "Basic"
			} else if strings.Contains(t.CardId, "gold") {
				//valid = currentTime.AddDate(0, 2, 0).Format("01-02-2006")
				valid = " "
				cardType = " "
				balance = "90"
				//cardType = "Gold"
			} else if strings.Contains(t.CardId, "diam") {
				//valid = currentTime.AddDate(0, 3, 0).Format("01-02-2006")
				valid = " "
				cardType = " "
				balance = "150"
				//cardType = "Diamond"
			} else if strings.Contains(t.CardId, "xxxx") {
				valid = " "
				cardType = " "
				balance = "300"
			}
			customerInfo, err := customerService.NewCustomerInfo(t.UserName, cardType, valid, balance, pushId)
			if err != nil {
				errMsg := err.Error()
				newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
				json.NewEncoder(w).Encode(&newStatusCode)
				return

			}
			customerService.CreateCustomerInfo(customerInfo)
			if err != nil {
				errMsg := err.Error()
				newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
				json.NewEncoder(w).Encode(&newStatusCode)
				return
			}

			var a = rechargeService.DeleteCardInfo(t)
			spew.Dump(a)
			if a != nil {
				errMsg := err.Error()
				newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
				json.NewEncoder(w).Encode(&newStatusCode)
				return
			}

			json.NewEncoder(w).Encode(statusCodes[7])

			return
		}
	}

	json.NewEncoder(w).Encode(statusCodes[6])
}
