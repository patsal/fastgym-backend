package api

type StatusCode struct {
	StatusCodeList StatusCodeList `json:"statusCode"`
}
type StatusCodeList struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
