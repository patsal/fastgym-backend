package api

type CardInfo struct {
	UserName string `json:"userName"`
	CardId   string `json:"cardId"`
	CardType string `json:"cardType"`
}
