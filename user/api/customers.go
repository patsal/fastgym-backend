package api

//Person struct
type Customer struct {
	/*	ID        string `json:"id"`*/
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	UserName  string `json:"userName"`
	Password  string `json:"password"`
	Address   string `json:"address"`
}

type CustomerInfo struct {
	UserName    string `json:"userName"`
	AccountType string `json:"accountType"`
	Valid       string `json:"valid"`
	Balance     string `json:"balance"`
	QrCode      string `json:"qrcode"`
	PushId      string `json:"pushId"`
}
