package api

type Discount struct {
	/*	ID        string `json:"id"`*/
	Id             string `json:"id"`
	Title          string `json:"title"`
	DateExpiration string `json:"dateExpiration"`
	Price          string `json:"price"`
	Priority       string `json:"priority"`
}

type Membership struct {
	/*	ID        string `json:"id"`*/
	Id       string `json:"id"`
	Title    string `json:"title"`
	Price    string `json:"price"`
	Priority string `json:"priority"`
}
