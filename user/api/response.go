package api

type Response struct {
	Data           interface{}    `json:"data"`
	ResponseStatus ResponseStatus `json:"statusCode"`
}

type ResponseStatus struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
