package api

type GymInfo struct {
	GymId       string `json:"gymId"`
	GymName     string `json:"gymName"`
	GymNumber   string `json:"gymNumber"`
	GymLocation string `json:"gymLocation"`
	GymTime     string `json:"gymTime"`
	GymX        string `json:"gymX"`
	GymY        string `json:"gymY"`
	Image       string `json:"image"`
}
