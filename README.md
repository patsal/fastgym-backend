#Fastgym Backend

## Get the code 

run:  `go get -u  bitbucket.org/patsal/fastgym-backend`

## Initialize the project

To initialize the project run `make init` in the project root directory

## Package manager

This project uses [glide](https://github.com/Masterminds/glide) for package management 

to install a new package/dependency use glide get: for example to install the github.com/gorilla/websocket package, you can do that: 

`glide get github.com/gorilla/websocket`

## Database used

This project uses SQLite3 as a database, shipped with the code base in the file `database.db`

