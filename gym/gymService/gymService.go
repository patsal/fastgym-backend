package gymService

import (
	"bitbucket.org/patsal/fastgym-backend/gym/gymApi"
	"bitbucket.org/patsal/fastgym-backend/gym/gymData"
	"github.com/davecgh/go-spew/spew"
	"strings"
)

type GymService interface {
	GetGymInformation() ([]gymApi.GymInformation, error)
	NewGymInformation(username string, password string) (gymApi.GymInformation, error)
}

type gymService struct{}

func (s gymService) GetGymInformation() ([]gymApi.GymInformation, error) {
	return gymData.GetGymInfo()
}

func (s gymService) isExistingCustomerInfo(username string) bool {
	customersInDb, _ := s.GetGymInformation()
	for index, customerInDb := range customersInDb {
		spew.Dump(index)
		spew.Dump(customerInDb.UserName)
		spew.Dump(strings.ToLower(strings.Trim(username, " ")))
		if customerInDb.UserName == sanitizeUsername(username) {
			return true
		}
	}
	return false
}

func NewGymService() GymService {
	//later on if MySqlDb dependency not present, throw error or panic
	return gymService{}
}

func (s gymService) NewGymInformation(username string, password string) (gymApi.GymInformation, error) {
	customerInfo := gymApi.GymInformation{
		UserName: username,
		Password: password,
		Valid:    1,
	}
	return customerInfo, nil
}

func sanitizeUsername(username string) string {
	return strings.ToLower(strings.Trim(username, " "))
}
