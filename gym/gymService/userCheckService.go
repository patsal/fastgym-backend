package gymService

import (
	"bitbucket.org/patsal/fastgym-backend/gym/gymApi"
	"bitbucket.org/patsal/fastgym-backend/gym/gymData"
	"fmt"
	"github.com/davecgh/go-spew/spew"
)

type UserCheckService interface {
	GetUserCheck() ([]gymApi.UserCheck, error)
	GetChannel() ([]gymApi.Channel, error)
	CreateUserCheck(userChecked gymApi.UserCheck) error
	NewUserCheck(username string, valid string, image string, accountType string, gymName string, checkedValid string, checkedValid2 string) (gymApi.UserCheck, error)
}

type userCheckService struct {
}

func (s userCheckService) GetUserCheck() ([]gymApi.UserCheck, error) {
	return gymData.GetUserCHeck()
}

func (s userCheckService) GetChannel() ([]gymApi.Channel, error) {
	return gymData.GetChannel()
}

func (s userCheckService) CreateUserCheck(userCheck gymApi.UserCheck) error {
	spew.Dump("CreateUserCheck")
	/*if s.isExistingUserCheckedInfo(userCheck.UserName, userCheck.ChekedValid) {
		return errors.New(userCheck.UserName + ": Already Checked in")
	}
	*/
	userCheck.UserName = sanitizeUsername(userCheck.UserName)
	err := gymData.CreateUserCheck(userCheck)
	if err != nil {
		fmt.Print("create error")
		fmt.Println(err)
		return err
	}
	return nil
}

func (s userCheckService) NewUserCheck(username string, valid string, image string, accountType string, gymName string, checkedValidDate string, checkedValidTime string) (gymApi.UserCheck, error) {
	userCheck := gymApi.UserCheck{
		UserName:         username,
		AccountType:      accountType,
		Valid:            valid,
		Image:            image,
		CheckedInto:      gymName,
		ChekedValidDate:  checkedValidDate,
		CheckedValidTime: checkedValidTime,
	}
	return userCheck, nil

}
func NewUserCheckService() UserCheckService {
	return userCheckService{}
}

/*func (s userCheckService) isExistingUserCheckedInfo(username string, checked string) bool {
	customersInDb, _ := s.GetUserCheck()
	for index, userCheckedInDb := range customersInDb {
		spew.Dump(index)
		spew.Dump(userCheckedInDb.UserName)
		spew.Dump(strings.ToLower(strings.Trim(username, " ")))
		if userCheckedInDb.UserName == sanitizeUsername(username) {
			if userCheckedInDb.ChekedValid == checked {
				return true
			}
		}
	}
	return false
}*/
