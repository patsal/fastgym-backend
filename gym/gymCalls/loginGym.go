package gymCalls

import (
	"encoding/json"
	"net/http"

	"github.com/davecgh/go-spew/spew"

	"bitbucket.org/patsal/fastgym-backend/gym/gymApi"
)

func LoginGym(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	decoder := json.NewDecoder(router.Body)
	var t gymApi.GymInformation
	decoder.Decode(&t)
	spew.Dump(t)
	gym, _ := gymServ.GetGymInformation()
	statusCode, _ := statusCodeService.GetStatusCode()
	for _, gym := range gym {
		spew.Dump(gym)
		if gym.UserName == t.UserName && gym.Password == t.Password {
			json.NewEncoder(w).Encode(statusCode[0])
			return
		}
	}
	json.NewEncoder(w).Encode(statusCode[1])
}
