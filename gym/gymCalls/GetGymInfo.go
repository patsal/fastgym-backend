package gymCalls

import (
	"bitbucket.org/patsal/fastgym-backend/gym/gymApi"
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"encoding/json"
	"github.com/davecgh/go-spew/spew"
	"net/http"

	"bitbucket.org/patsal/fastgym-backend/user/service"
)

var gymServices = service.NewGymsService()
var gymsInfo, _ = gymServices.GetGymInfo()

func GetGymInfo(w http.ResponseWriter, router *http.Request) {
	spew.Dump(gymsInfo)
	w.Header().Set("Content-Type", "application/json")
	var t gymApi.GymInformation
	err := json.NewDecoder(router.Body).Decode(&t)
	if err != nil {
		json.NewEncoder(w).Encode(statusCodes[5])
		return
	}
	spew.Dump(t)
	statusCode, _ := statusCodeService.GetStatusCode()
	for _, item := range gymsInfo {
		if item.GymName == t.UserName {
			response := api.Response{
				ResponseStatus: api.ResponseStatus{
					Code:    0,
					Message: "Success",
				},
				Data: item,
			}

			json.NewEncoder(w).Encode(response)

			return
		}
	}

	json.NewEncoder(w).Encode(statusCode[4])
}
