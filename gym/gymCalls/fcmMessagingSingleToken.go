package gymCalls

import (
	"bitbucket.org/patsal/fastgym-backend/fcm"
	"encoding/json"
	"fmt"
	"net/http"
)

func FCloudMessaging(username string, pushId string, message string, code string, w http.ResponseWriter) {
	data := map[string]string{
		"message": message,
		"title":   "Dear, " + username,
		"code":    code,
	}

	notificationOnBackGround := &fcm.NotificationPayload{Title: username, Body: message}

	ids := []string{
		pushId,
	}
	c := fcm.NewFcmClient(serverKey)

	c.NewFcmRegIdsMsg(ids, data)
	c.SetNotificationPayload(notificationOnBackGround)

	status, err := c.Send()

	if err == nil {
		status.PrintResults()
		if status.StatusCode != 200 {
			json.NewEncoder(w).Encode(statusCodes[8])
			return
		}

		return
	} else {
		fmt.Println(err)
	}
}
