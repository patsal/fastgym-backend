package gymCalls

import (
	"bitbucket.org/patsal/fastgym-backend/user/api"
	"encoding/json"
	"github.com/davecgh/go-spew/spew"
	"net/http"
	"time"
)

const (
	//application fcm key
	serverKey = "AIzaSyAUIGmQHqS5NJXRxyqbQsOIOsxvMm4sZD8"
)

func NotifyUser(w http.ResponseWriter, router *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	var t api.CustomerInfo

	var gymName = router.Header.Get("gymName")

	err := json.NewDecoder(router.Body).Decode(&t)

	if err != nil {
		json.NewEncoder(w).Encode(statusCodes[5])
		return
	}

	customerInfo, _ := customerService.GetCustomersInfo()

	for _, db := range customerInfo {

		if db.QrCode == t.QrCode {

			timeDb, _ := time.Parse(dateLayout, db.Valid)

			cu := time.Now()

			if timeDb.Before(cu) && db.Valid != currentTime {
				FCloudMessaging(db.UserName, db.PushId, fcmMsgExpired, codeExp, w)
				json.NewEncoder(w).Encode(statusCodes[13])
				return
			}

			if len(db.PushId) < 2 {
				spew.Dump("push id less than 2 len")
				return
			}

			userChecked, err := userCheckedService.NewUserCheck(db.UserName, db.Valid, "not done yet", db.AccountType, gymName, currentTime, checkedTime)
			if err != nil {
				errMsg := err.Error()
				newStatusCode := statusCodeService.NewStatusCode(400, errMsg)
				json.NewEncoder(w).Encode(&newStatusCode)
				return
			}

			errorChecked := userCheckedService.CreateUserCheck(userChecked)
			if errorChecked != nil {
				errMsg := errorChecked.Error()
				newStatusCode := statusCodeService.NewStatusCode(206, errMsg)
				FCloudMessaging(db.UserName, db.PushId, fcmMsgChecked, codeChkd, w)
				json.NewEncoder(w).Encode(&newStatusCode)
				return
			}
			FCloudMessaging(db.UserName, db.PushId, fcmMsgAccepted, codeAcp, w)
			json.NewEncoder(w).Encode(statusCodes[11])
			return

		}

	}
	json.NewEncoder(w).Encode(statusCodes[12])
}
