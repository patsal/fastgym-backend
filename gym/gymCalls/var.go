package gymCalls

import (
	"bitbucket.org/patsal/fastgym-backend/gym/gymService"
	"bitbucket.org/patsal/fastgym-backend/user/service"
	"time"
)

var gymServ = gymService.NewGymService()
var statusCodeService = service.NewStatusCodeService()
var currentTime = time.Now().Format(dateLayout)
var checkedTime = time.Now().Format(timeLayout)
var customerService = service.NewCustomersService()
var statusCodes, _ = statusCodeService.GetStatusCode()
var userCheckedService = gymService.NewUserCheckService()

var dateLayout = "01-02-2006"
var timeLayout = "3:04pm"
var fcmMsgAccepted = "You can fully enter the gym"
var codeAcp = "0"
var codeExp = "1"
var codeChkd = "2"
var fcmMsgChecked = "You cannot enter, Already checked in"
var fcmMsgExpired = "Your account is expired"
