package gymData

import (
	"bitbucket.org/patsal/fastgym-backend/gym/gymApi"
	"errors"
	"github.com/davecgh/go-spew/spew"
	"time"
)

var userCheck []gymApi.UserCheck
var channel []gymApi.Channel

func init() {
	setUserCHeck()
}
func GetUserCHeck() ([]gymApi.UserCheck, error) {

	return userCheck, nil

}

func GetChannel() ([]gymApi.Channel, error) {

	return channel, nil

}

func setUserCHeck() error {

	userCheck = append(userCheck, gymApi.UserCheck{UserName: "patrick", AccountType: "diamond", Valid: "", CheckedInto: "Atlantis", CheckedValidTime: "", ChekedValidDate: "10-14-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "rita", AccountType: "basic", Valid: "12-1-2018", CheckedInto: "ClassSport", ChekedValidDate: "10-12-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "selim", AccountType: "Gold", Valid: "12-1-2018", CheckedInto: "ClassSport", ChekedValidDate: "10-12-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "omar", AccountType: "basic", Valid: "12-1-2018", CheckedInto: "Atlantis", CheckedValidTime: "1:44pm", ChekedValidDate: "6-13-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "yousef", AccountType: "basic", Valid: "12-1-2018", CheckedInto: "Atlantis", CheckedValidTime: "7:54pm", ChekedValidDate: "7-19-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "pascal", AccountType: "basic", Valid: "12-1-2018", CheckedInto: "Atlantis", CheckedValidTime: "4:44pm", ChekedValidDate: "8-13-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "rony", AccountType: "basic", Valid: "12-1-2018", CheckedInto: "Atlantis", CheckedValidTime: "9:44pm", ChekedValidDate: "9-13-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "marcel", AccountType: "basic", Valid: "12-1-2018", CheckedInto: "Atlantis", CheckedValidTime: "11:44pm", ChekedValidDate: "10-09-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "joseph", AccountType: "basic", Valid: "12-1-2018", CheckedInto: "Atlantis", CheckedValidTime: "10:31am", ChekedValidDate: "10-15-2018"})
	userCheck = append(userCheck, gymApi.UserCheck{UserName: "joseph.s", AccountType: "basic", Valid: "12-1-2018", CheckedInto: "Atlantis", CheckedValidTime: "1:21pm", ChekedValidDate: "10-15-2018"})

	return nil
}
func CreateUserCheck(userChecked gymApi.UserCheck) error {
	for _, item := range userCheck {
		if item.UserName == userChecked.UserName {
			spew.Dump("-----------------user found------------------")

			savedDate, _ := time.Parse("01-02-2006", item.ChekedValidDate)
			newDate, _ := time.Parse("01-02-2006", userChecked.ChekedValidDate)

			if savedDate == newDate {
				spew.Dump("-----------------same day-----------------")
				return errors.New(item.CheckedInto + " " + "User already checked in today at" + " " + userChecked.CheckedValidTime)
			}
		}
	}
	userCheck = append(userCheck, userChecked)
	return nil
}
