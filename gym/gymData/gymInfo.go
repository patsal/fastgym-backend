package gymData

import (
	"bitbucket.org/patsal/fastgym-backend/gym/gymApi"
	"fmt"
)

var gymInfo []gymApi.GymInformation

func init() {
	setGymInfo()
	fmt.Println("gym:gymInfo module initializing.... OK")
}

func GetGymInfo() ([]gymApi.GymInformation, error) {

	return gymInfo, nil
}

func setGymInfo() error {
	gymInfo = append(gymInfo, gymApi.GymInformation{GymId: "001", UserName: "atlantis", Password: "123", Valid: 1, Schedule: "123"})
	gymInfo = append(gymInfo, gymApi.GymInformation{GymId: "002", UserName: "classSport", Password: "123", Valid: 1, Schedule: "123456"})
	gymInfo = append(gymInfo, gymApi.GymInformation{GymId: "003", UserName: "FitnessZone", Password: "123", Valid: 1, Schedule: "123456"})
	return nil
}
