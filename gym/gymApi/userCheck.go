package gymApi

type UserCheck struct {
	UserName         string `json:"userName"`
	AccountType      string `json:"accountType"`
	Valid            string `json:"valid"`
	Image            string `json:"image"`
	CheckedInto      string `json:"checkedInto"`
	ChekedValidDate  string `json:"chekedValidDate"`
	CheckedValidTime string `json:"chekedValidTime"`
}
type Channel struct {
	UserName         string `json:"userName"`
	AccountType      string `json:"accountType"`
	Valid            string `json:"valid"`
	Image            string `json:"image"`
	CheckedInto      string `json:"checkedInto"`
	ChekedValidDate  string `json:"chekedValidDate"`
	CheckedValidTime string `json:"chekedValidTime"`
}
