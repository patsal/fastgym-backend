package gymApi

type GymInformation struct {
	GymId    string `json:"gymId"`
	UserName string `json:"userName"`
	Password string `json:"password"`
	Valid    int    `json:"valid"`
	Schedule string `json:"schedule"`
}
