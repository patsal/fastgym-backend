package main

import (
	"bitbucket.org/patsal/fastgym-backend/gym/gymCalls"
	"bitbucket.org/patsal/fastgym-backend/user/calls"
	"database/sql"
	"github.com/davecgh/go-spew/spew"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

const (
	STATIC_DIR = "/user/static/"
	serverKey  = "AIzaSyAUIGmQHqS5NJXRxyqbQsOIOsxvMm4sZD8"
	topic      = "/topics/someTopic"
)

func getprivacyPolicy(w http.ResponseWriter, router *http.Request) {
	http.ServeFile(w, router, "index.html")
}

func main() {
	// Init router

	db, err := sql.Open("mysql", "root:password@tcp(127.0.0.1:3306/fastgymdb")
	if err != nil {
		panic(err.Error())
	}

	defer db.Close()

	router := mux.NewRouter().StrictSlash(true)

	router.PathPrefix(STATIC_DIR).Handler(http.StripPrefix(STATIC_DIR, http.FileServer(http.Dir("."+STATIC_DIR))))

	//just for checking

	router.HandleFunc("/GiveMeAllTheCustomers/customers", calls.GetCustomers).Methods("GET")
	router.HandleFunc("/GiveMeAllTheCustomers/customer/{userName}", calls.GetCustomer).Methods("GET")

	router.HandleFunc("/api/gym/userCheck", gymCalls.UserCheck).Methods("GET")

	router.HandleFunc("/api/privacy", getprivacyPolicy).Methods("GET")
	router.HandleFunc("/api/user/GetUserInfo", calls.GetCustomersInfo).Methods("GET")
	router.HandleFunc("/api/user/GetGymDetails", calls.GetGymDetails).Methods("GET")
	router.HandleFunc("/api/user/GetDiscountInfo", calls.GetDiscountInfo).Methods("GET")
	router.HandleFunc("/api/user/GetMembershipInfo", calls.GetMembershipInfo).Methods("GET")
	//router.HandleFunc("/api/people/{id}", updateCustomer).Methods("PUT")
	/* router.HandleFunc("/api/people/{id}", deletePerson).Methods("DELETE")*/
	router.HandleFunc("/api/user/Login", calls.LoginUser).Methods("POST")
	router.HandleFunc("/api/user/customers", calls.CreateCustomer).Methods("POST")
	router.HandleFunc("/api/user/GetUserInfo", calls.GetCustomerInfo).Methods("POST")
	router.HandleFunc("/api/user/Recharge", calls.Recharge).Methods("Post")
	router.HandleFunc("/api/user/DeleteCustomer", calls.Recharge).Methods("Post")
	router.HandleFunc("/api/user/BuyMembership", calls.BuyMembership).Methods("Post")

	router.HandleFunc("/api/gym/NotifyUser", calls.BuyMembership).Methods("Post")

	//Gym

	router.HandleFunc("/api/gym/getGymInfo", gymCalls.GetGymInfo).Methods("POST")
	router.HandleFunc("/api/gym/notifyUser", gymCalls.NotifyUser).Methods("POST")
	router.HandleFunc("/api/gym/Login", gymCalls.LoginGym).Methods("POST")

	spew.Dump("Booting webserver on port 8000")

	log.Fatal(http.ListenAndServe(":8000", router))

}
