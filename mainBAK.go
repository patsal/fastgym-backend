package main

/*
import (
	"encoding/json"
	"flag"
	"github.com/gorilla/mux"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

const httpServerAddress = "0.0.0.0:8000"

//Person struct
type Person struct {
	ID        string   `json:"id"`
	Firstname string   `json:"firstname"`
	Lastname  string   `json:"lastname"`
	Address   *Address `json:"address"`
}

type Address struct {
	Country string `json:"country"`
	State   string `json:"state"`
	City    string `json:"city"`
}

//get all people
func getPeople(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(people)
}

//get single person
func getPerson(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(router) //get Params
	//loop through books and find with id
	for _, item := range people {
		if item.ID == params["id"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&Person{})
}

//create new person
func createPerson(w http.ResponseWriter, router *http.Request) {
	var person Person
	_ = json.NewDecoder(router.Body).Decode(&person)
	person.ID = strconv.Itoa(rand.Intn(100000000000)) // ID not safe
	people = append(people, person)
	json.NewEncoder(w).Encode(person)
}

//update person
func updatePerson(w http.ResponseWriter, router *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(router)
	for index, item := range people {
		if item.ID == params["id"] {
			people = append(people[:index], people[index+1:]...)
			var person Person
			_ = json.NewDecoder(router.Body).Decode(&person)
			person.ID = params["id"] // ID not safe
			people = append(people, person)
			json.NewEncoder(w).Encode(person)
			return
		}
	}
	json.NewEncoder(w).Encode(people)
}

//delete person
func deletePerson(w http.ResponseWriter, router *http.Request) {

	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(router)
	for index, item := range people {
		if item.ID == params["id"] {
			people = append(people[:index], people[index+1:]...)
			break
		}
	}
	json.NewEncoder(w).Encode(people)
}

var people []Person

func main() {
	var wait time.Duration
	flag.DurationVar(&wait, "graceful-timeout", time.Second*15, "the duration for which the server gracefully wait for existing connections to finish - e.g. 15s or 1m")
	flag.Parse()

	// Init router
	router := mux.NewRouter()

	// Mock data - implement MySqlDb
	people = append(people, Person{ID: "1", Firstname: "Patrick", Lastname: "Saber", Address: &Address{Country: "Lebanon", State: "Kesrouwan", City: "Jounieh"}})
	people = append(people, Person{ID: "2", Firstname: "Salim", Lastname: "Saber", Address: &Address{Country: "Germany", State: "Berlin", City: "Berlin"}})
	people = append(people, Person{ID: "2", Firstname: "Salim", Lastname: "Saber", Address: &Address{Country: "Germany", State: "Berlin", City: "Berlin"}})
	// Router handlers/endpoint
	router.HandleFunc("/api/people", getPeople).Methods("GET")
	router.HandleFunc("/api/people/{id}", getPerson).Methods("GET")
	router.HandleFunc("/api/people", createPerson).Methods("POST")
	router.HandleFunc("/api/people/{id}", updatePerson).Methods("PUT")
	router.HandleFunc("/api/people/{id}", deletePerson).Methods("DELETE")

	srv := &http.Server{
		Addr: httpServerAddress,
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      router, // Pass our instance of gorilla/mux in.
	}

	log.Print("Running Webserver on port " + httpServerAddress)
	if err := srv.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
*/
